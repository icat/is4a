/**
 * MongoDB schema
 */
/** @module mongoDBSchema */

const mongoose = require('mongoose');

/** Event schema */
const JobSchema = mongoose.Schema({       
    /** Type of the job */
    type: String,
    /** Aunthentication token */
    token: String,
    /** Aunthentication token */
    username: String,
    /** status of the job */
    status: String,       
    /** Job's input */
    input : mongoose.Schema.Types.Mixed,
    /** Job's output */
    output : mongoose.Schema.Types.Mixed,
    /** Historic Job's input */
    inputs : [mongoose.Schema.Types.Mixed], 
    /** Job's input */
    onHold : [mongoose.Schema.Types.Mixed], 
    /** Keep historic of status */
    statuses: [{ status: String, date: Date }],
    /** Verbose log messages */
    log : [{ text: String, date: Date }]
}, {
    timestamps: true
});

module.exports = mongoose.model('Job', JobSchema);