module.exports = (app) => {

    
    const controller = require('../controllers/job.controller.js');
    const auth = require('../authentication/authenticator.js');

    /**
    * @swagger
    * /jobs:
    *   get:
    *     summary: Returns all jobs
    *     responses:
    *       '200':
    *         content:
    *           application/json:
    *             schema:
    *               $ref: '#/components/schemas/job'  
    *     tags:
    *       - JOB
    */
    app.get('/jobs', auth.validateSession, controller.getAll);

    /**
    * @swagger
    * /jobs:
    *   post:
    *     summary: Creates a new job
    *     requestBody:
    *       $ref: '#/components/requestBodies/createJob'
    *     responses:
    *       '200':
    *         content:
    *           application/json:
    *             schema:
    *               $ref: '#/components/schemas/job'  
    *     tags:
    *       - JOB
    */
   app.post('/jobs', auth.validateSession, controller.create);

    /**
    * @swagger
    * /jobs/{id}/status/{status}:
    *   get:
    *     summary: Returns allowed types of job
    *     responses:
    *       '200':
    *         description: OK 
    *     tags:
    *       - JOB
    */
   app.get('/jobs/:id/status/:status', auth.validateSession, controller.updateStatus);

    /**
    * @swagger
    * /jobs/type/{category}/entity/{entityTarget}:
    *   get:    
    *     summary: Gets the list of jobs that are applicable to a category and entity. A entity is a class.
    *     parameters:
    *       - in: path
    *         name: category
    *         required: true
    *         description: Category of the type of jobs usually organized by technique. Example mx, saxs
    *         example : mx
    *         schema:
    *           type: string
    *       - in: path
    *         name: entityTarget
    *         required: true
    *         description: Identifier of the entity associated to a job. Example datacollectiongroup, datacollection
    *         example : datacollection
    *         schema:
    *           type: string
    *     responses:
    *       '200':
    *         description: OK 
    *     tags:
    *       - JOB
    */
   app.get('/jobs/type/:category/:entityTarget',  controller.getTypesByEntityTarget);


    /**
    * @swagger
    * /jobs/{id}/resume:
    *   post:
    *     summary: Add a onhold form to a job
    *     responses:
    *       '200':
    *         description: OK 
    *     tags:
    *       - JOB
    */
   app.post('/jobs/:id/resume', auth.validateSession, controller.resume);

     /**
    * @swagger
    * /jobs/{id}/onhold:
    *   post:
    *     summary: Add a onhold form to a job
    *     responses:
    *       '200':
    *         description: OK 
    *     tags:
    *       - JOB
    */
   app.post('/jobs/:id/onhold', auth.validateSession, controller.onHold);

    /**
    * @swagger
    * /jobs/{id}/output:
    *   post:
    *     summary: Add output to the job
    *     responses:
    *       '200':
    *         description: OK 
    *     tags:
    *       - JOB
    */
   app.post('/jobs/:id/output', auth.validateSession, controller.appendResults);


    /**
    * @swagger
    * /jobs/{username}:
    *   get:
    *     summary: Gets all jobs from user
    *     responses:
    *       '200':
    *         description: OK 
    *     tags:
    *       - JOB
    */
   app.get('/jobs/:username', controller.getJobsByUser);


   
     /**
     * @swagger
     * /jobs/upload:
     *   post:
     *     summary: Uploads a file     
     *     responses:
     *       '200':
     *         description: 'Ok'
     *     tags:
     *       - JOB
     */   
    app.post('/upload',  controller.upload);

     /**
     * @swagger
     * /{id}/download:
     *   get:
     *     summary: Downloads a file
     *     responses:
     *       '200':
     *         description: 'Ok'
     *     tags:
     *       - JOB
     */   
    app.get('/:id/download',  controller.download);

       /**
    * @swagger
    * /jobs/{id}/log:
    *   post:    
    *     summary: Add a new log message to a job.
    *     parameters:
    *       - in: body
    *         name: log    
    *         description: Category of the type of jobs usually organized by technique. Example mx, saxs
    *         example : This is a log
    *         schema:
    *           type: string
    *       - in: path
    *         name: id
    *         required: true
    *         description: Identifier of the job
    *         schema:
    *           type: string
    *     responses:
    *       '200':
    *         description: OK 
    *     tags:
    *       - JOB
    */
   app.post('/jobs/:id/log', auth.validateSession, controller.log);
}