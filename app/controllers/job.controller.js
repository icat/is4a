

const Job = require('../models/job.model.js');
const JobTypes = require('../../config/job.types.config.js');


// Create a storage object with a given configuration
const storage = require('multer-gridfs-storage')({
    url: global.gConfig.database.uri,
    file: (req, file) => {
        global.gLogger.debug("file", { file : file  });
        return {
            filename: file.originalname
        };
    }
});

var multer = require('multer');
const upload = multer({ storage: storage }).single('file');

exports.upload = (req, res) => {
    global.gLogger.debug("Upload file", {  });
    upload(req, res, function (err) {        
        if (err) {
            res.json({ error_code: 1, err_desc: err });
            return;
        }
        global.gLogger.debug("File has been uploaded", { id : req.file.id });
        res.send(req.file.id);       
    });
};




exports.getJobsByUser = (req, res) => { 
    try {
        global.gLogger.info("getJobsByUser", { username : req.params.username });
        if (!req.params.username){
            global.gLogger.error("Username is mandatory");
            throw new Error("Username is mandatory");
        }

        Job.find({
            username : req.params.username
        })
        .then(events => {
            res.send(events);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving jobs."
            });
        });
    }
    catch (error) {
        global.gLogger.error(error);
    }
};

exports.getAll = (req, res) => { 
    try {
        Job.find()
        .then(events => {
            res.send(events);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving jobs."
            });
        });
    }
    catch (error) {
        global.gLogger.error(error);
    }
};


exports.getTypesByEntityTarget = (req, res) => {
    try {
        res.send(JobTypes.types[req.params.category][req.params.entityTarget]);        
    }
    catch (error) {
        global.gLogger.error(error);
    }
};




/**
 * Download a file given a eventId. 
 *
 **/
exports.download = (req, res) => {
    var gridfs = require('mongoose-gridfs')({
        collection: 'fs'
        //model: 'Attachment'
    });        
    let comingStream = gridfs.model.readById(req.params.id);
    comingStream.pipe(res);
    comingStream.on('error', function (err) {
        console.log(err);
    });    

};


exports.create = (req, res) => {
    try {
        //global.gLogger.info("create", { body : JSON.stringify(req.body) });
        if (!req.body.type){
            global.gLogger.error("Type of job is mandatory");
            throw new Error("Type of job is mandatory");
        }
        if (!req.body.token){
            global.gLogger.error("Token mandatory");
            throw new Error("Token is mandatory");
        }
        if (!req.body.username){
            global.gLogger.error("Username mandatory");
            throw new Error("Username is mandatory");
        }
        
        const job = new Job({   
            status : "PENDING",        
            token: req.body.token,            
            type: req.body.type.toUpperCase() || null,            
            input: req.body.input || null,
            inputs : [req.body.input || null],
            username: req.body.username || null            
        });
        job.save().then(data => {
            global.gLogger.debug("Job has been created successfully");
            res.send(data);
        }).catch(err => {
            global.gLogger.error(err);
            res.status(500).send({
                message: err.message || "Some error occurred while creating the job."
            });
        }); 

    }
    catch (error) {
        res.status(500).send({
            message: error.message || "Some error occurred while creating the job."
        });
    }
};

exports.log = (req, res) => {   
    try {
        if (!req.params.id){
            throw new Error("id is mandatory");
        }
        global.gLogger.info("log", { id: req.params.id, log: req.body.log });

        Job.findById(req.params.id).then(job => {           
            job.log.push({ text: req.body.log, date: Date.now() });
            job.save()
                .then(data => {
                    global.gLogger.info("Added log" + req.body.log, {id : job._id} );
                    res.send(data);
                }).catch(err => {
                    global.gLogger.error(err);
                    res.status(500).send({
                        message: err.message || "Some error occurred while updating the status of a job"
                    });
                });
    
        }).catch(err => {
            console.log(err)
            global.gLogger.error("Job not found" , {id : req.params.id} );
            res.status(500).send({
                message: err.message || "Job not found"
            });
        });
    }
    catch (error) {
        global.gLogger.error(error);
    }
};

exports.updateStatus = (req, res) => {   
    try {
        if (!req.params.id){
            throw new Error("id is mandatory");
        }
        global.gLogger.info("updateStatus", { id: req.params.id, status: req.params.status });

        Job.findById(req.params.id).then(job => {           
            let currentStatus = job.status;
            job.status = req.params.status.toUpperCase();
            job.statuses.push({
                status  : job.status.toUpperCase(),
                date    : Date.now()
            })
            job.save()
                .then(data => {
                    global.gLogger.info("Job status has been modified " + currentStatus + " => " + job.status, {id : job._id} );
                    res.send(data);
                }).catch(err => {
                    global.gLogger.error(err);
                    res.status(500).send({
                        message: err.message || "Some error occurred while updating the status of a job"
                    });
                });
    
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Job not found"
            });
        });
    }
    catch (error) {
        global.gLogger.error(error);
    }
};

exports.resume = (req, res) => {   
    try {
        if (!req.params.id){
            throw new Error("id is mandatory");
        }
        
        global.gLogger.info("resume", { id: req.params.id, input: JSON.stringify(req.body) });

        Job.findById(req.params.id).then(job => {                       
            global.gLogger.info("Job has been found", { id: req.params.id});
            /** This save the input in the history */
            job.inputs.push(job.input);            
            job.input = req.body.input;
            job.token = req.body.token;
            job.status = "RESUME";
            job.save()
                .then(data => {
                    global.gLogger.info("Job input has been updated", {id : job._id} );
                    res.send(data);
                }).catch(err => {
                    global.gLogger.error(err);
                    res.status(500).send({
                        message: err.message || "Some error occurred while updating the onHold of a job"
                    });
                });
    
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Job not found"
            });
        });
    }
    catch (error) {
        global.gLogger.error(error);
    }
};


exports.onHold = (req, res) => {   
    try {
        if (!req.params.id){
            throw new Error("id is mandatory");
        }
        
        global.gLogger.info("onHold", { id: req.params.id, input: JSON.stringify(req.body) });

        Job.findById(req.params.id).then(job => {                       
            global.gLogger.info("Job has been found", { id: req.params.id});
            /** This save the input in the history */
            //job.inputs.push(job.input);            
            job.onHold.push(req.body.schema);
            job.inputs.push(job.input);            
            job.input = req.body.input;
                        
            job.status = "ONHOLD";
            job.save()
                .then(data => {
                    global.gLogger.info("Job input has been updated", {id : job._id} );
                    res.send(data);
                }).catch(err => {
                    global.gLogger.error(err);
                    res.status(500).send({
                        message: err.message || "Some error occurred while updating the onHold of a job"
                    });
                });
    
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Job not found"
            });
        });
    }
    catch (error) {
        global.gLogger.error(error);
    }
};

exports.appendResults = (req, res) => {   
    try {
        if (!req.params.id){
            throw new Error("id is mandatory");
        }
        
        global.gLogger.info("appendResults", { id: req.params.id, result: JSON.stringify(req.body) });

        Job.findById(req.params.id).then(job => {                       
            job.output = req.body;
    
            job.save()
                .then(data => {
                    global.gLogger.info("Job output has been updated", {id : job._id} );
                    res.send(data);
                }).catch(err => {
                    global.gLogger.error(err);
                    res.status(500).send({
                        message: err.message || "Some error occurred while updating the output of a job"
                    });
                });
    
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Job not found"
            });
        });
    }
    catch (error) {
        global.gLogger.error(error);
    }
};
