FROM surnet/alpine-node-wkhtmltopdf:9.11.2-0.12.5-small

ENV NODE_ENV production

WORKDIR /is4a
COPY . /is4a

RUN apk --no-cache add --virtual build-dependencies git \
  && npm install \
  && apk del build-dependencies \
  && apk --no-cache add curl bash

EXPOSE 8000

ENTRYPOINT ["/bin/sh", "/is4a/entrypoint"]
CMD ["npm", "start"]

HEALTHCHECK --start-period=60s --timeout=20s --retries=2 CMD /is4a/healthcheck
