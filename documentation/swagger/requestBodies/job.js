/**
 * @swagger
 *
 * components:
 *   requestBodies:
 *     createJob:
 *       description: Job to be created
 *       content:
 *         'application/json':
 *           schema:
 *             $ref: '#/components/schemas/job'
 *     updateTag:
 *       description: updated tag
 *       content:
 *         'application/json':
 *           schema:
 *             $ref: '#/components/schemas/tag'
 */