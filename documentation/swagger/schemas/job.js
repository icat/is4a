/**
 * @swagger
 *
 * components:
 *   schemas:
 *     job:
 *       required: 
 *         - "id"
 *       properties: 
 *         id: 
 *           type: "string"
 *         createTime: 
 *           type: "string"
 *         type: 
 *           type: "string"
 *         status: 
 *           type: "string"
 *         input: 
 *           type: "object"
 *         output: 
 *           type: "object"
 */
    