module.exports = {
        result : {
            success: [
                {                             
               
                    resultA : 2450923,
                    resultB : 52,
                    resultC : 300
                
            }]
        },
        create: {
            success: [{             
                type: "add",
                token : "4b09aacd1709fc29085aa3ed5d6f2d250ee050b8",
                username : "tester",
                input: {
                    datacollectionid : 2450923,
                    a : 52,
                    b : 300
                }
            },
            {             
                type: "autoproc",
                token : "4b09aacd1709fc29085aa3ed5d6f2d250ee050b8",
                username : "tester",
                input: {
                    datacollectionid : 2450923,
                    start : 52,
                    end : 300,
                    resolution : 2.5
                }
            },
            {               
                type: "autoproc",
                token : "4b09aacd1709fc29085aa3ed5d6f2d250ee050b8",
                username : "tester",
                input: {
                    datacollectionid : 2450923,
                    start : 52,
                    end : 300,
                    spaceGroup : "P1"
                }
            },
            {             
                type: "autoproc",
                token : "4b09aacd1709fc29085aa3ed5d6f2d250ee050b8",
                username : "tester",
                input: {
                    datacollectionid : 2450923,
                    start : 52,
                    end : 300,
                    spaceGroup : "P1",
                    resolution : 2.5
                }
            }],
            error: [
                {             
                    /** no type */                    
                    token : "4b09aacd1709fc29085aa3ed5d6f2d250ee050b8",
                    username : "tester",
                    input: {
                        datacollectionid : 2450923,
                        start : 52,
                        end : 300,
                        spaceGroup : "P1",
                        resolution : 2.5
                    }
                },
                {                                 
                    type: "autoproc",
                    username : "tester",
                    input: {
                        datacollectionid : 2450923,
                        start : 52,
                        end : 300,
                        spaceGroup : "P1",
                        resolution : 2.5
                    }
                },
                {             
                    type: "autoproc",
                    token : "4b09aacd1709fc29085aa3ed5d6f2d250ee050b8",                   
                    input: {
                        datacollectionid : 2450923,
                        start : 52,
                        end : 300,
                        spaceGroup : "P1",
                        resolution : 2.5
                    }
                }

            ]

        }
};
