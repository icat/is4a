
global.gResource = require("./resources/job.resource.js");
global.chai = require('chai');
global.expect = require('chai').expect;
global.chaiHttp = require('chai-http');
global.should = chai.should();
global.chai.use(chaiHttp);

global.mongoUnit = require('mongo-unit');

process.env.NODE_ENV = 'test'


before(function (done) {

    // starts a mongodb instance which will stay in memory
    try{
        
        
        global.mongoUnit.start({ verbose: true  })
            .then(mongoUri => {
                console.log('Started mongoDB for all tests. URI is : ' + mongoUri);
                process.env.MONGO_URI = mongoUri;

                //initialize the icat+ server 
                global.gServer = require("../server.js");
                
                global.gRequester = chai.request(global.gServer).keepOpen();
                console.log("Server is listening...") 
                setTimeout(function () { done(); }, 5000);               
               
                
            })
            .catch(e => {
                console.log(e);
                console.log("1 Could not start the mongo instance for tests.")
                
            });
    }catch(e){
            console.log("2 Could not start the mongo instance for tests.")
            console.log(e);
          
    };
});

after(() => {
    
    global.gRequester.close();
    console.log("Server is closed")
    global.mongoUnit.stop();
    console.log("MongoDB is stopped.");

})


beforeEach(function () {   
});





