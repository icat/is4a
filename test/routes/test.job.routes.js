
require('it-each')({ testPerIteration: true });
let jobResource = require('../resources/job.resource.js');



describe('Job', () => {
    describe('Create', () => {                           
         it.each(jobResource.create.success, '[Create jobs id %s]', ['type'], function (element, next) {
            global.gRequester
                .post('/jobs')
                .set('Content-Type', 'application/json')
                .send(element)
                .end((err, res) => {                                     
                    expect(res.status).to.equal(200);                                                
                    next();
                })

        });
        
        it.each(jobResource.create.error, '[Create jobs]', [], function (element, next) {
            global.gRequester
                .post('/jobs')
                .set('Content-Type', 'application/json')
                .send(element)
                .end((err, res) => {                                     
                    expect(res.status).to.equal(500);                            
                    next();
                })

        });
  
    });


    describe('Update status', () => {            
       it('Update status of a single job to STARTING', function() {
            global.gRequester
                .get('/jobs')
                .set('Content-Type', 'application/json')                
                .end((err, res) => {                                         
                    global.gRequester
                            .get('/jobs/' + res.body[0]._id + "/status/STARTING")
                            .set('Content-Type', 'application/json')
                            .send()
                            .end((err, res) => {                                     
                                expect(res.status).to.equal(200);                                                            
                            })            
                    
                });
          })                                       
    });

    describe('Get Job by username', () => {            
        it('tester', function() {
             global.gRequester
                 .get('/jobs/tester')
                 .set('Content-Type', 'application/json')                
                 .end((err, res) => {                                                                                                 
                    expect(res.status).to.equal(200);                                                                                                      
                 });
        });                                              
     });

    describe('Get Job Types', () => {            
        it('datacollection', function() {
             global.gRequester
                 .get('/jobs/type/datacollection')
                 .set('Content-Type', 'application/json')                
                 .end((err, res) => {                                                                                                 
                    expect(res.status).to.equal(200);                                                                                                      
                 });
        });


        it('datacollectiongroup', function() {
            global.gRequester
                .get('/jobs/type/datacollectiongroup')
                .set('Content-Type', 'application/json')                
                .end((err, res) => {                                                                                                 
                   expect(res.status).to.equal(200);                                                                                                      
                });
       });                                          
     });


    describe('Processing', () => {         
        it('add', function() {
            global.gRequester
                .get('/jobs')
                .set('Content-Type', 'application/json')
                .send()
                .end((err, res) => {                                     
                    /** Get add jobs */
         
                    for (var i =0; i < res.body.length; i++){
                        var job = res.body[i];
                        
                        if (job.type == "ADD"){
                            debugger
                            var result = job.a + job.big;
                            console.log(result)
                            expect(result).to.equal(32);         
                        }
                    }


                });

            });

    });


   

});
