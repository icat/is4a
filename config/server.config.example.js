/**
 * ICAT+ configuration file
 */
/** @module confguration */
module.exports = {
    /** ICAT+ server configuration section */
    server: {
        /** HTTP port used by ICAT+ */
        port: 8000,
        /** ICAT+ URL. Used by the PDF generator */
        url: 'https://icatplus.esrf.fr',
        /** API_KEY to short circuit default authentication mechanism. Distribute this key to trusted clients. */
        API_KEY: '[API ALLOWS TO STORE NOTIFICATIONS IN THE LOGBOOK WITH NO AUTH]'
    },

    /** Database configuration section */
    database: {
        /** URI to connect to mongoDB */
        uri: 'mongodb://[USER]:[PASSWORD]@[HOST]:[PORT]/[DATABASE]?authSource=admin'
    },

    /** Elastic search configuration section */
    elasticsearch : {
        /** If enabled elastic search will send queries to the server */
        enabled : true,
        /** Server and pord where elasticsearch is running */
        server : "http://[SERVER]:[PORT]"
    },
    /** Logging configuration section */
    logging: {
        /** Logging to graylog */
        graylog: {
            /** If TRUE enables the sending of logs to graylog server */
            enabled: true,
            /** Graylog server URL */
            host: 'graylog-dau.esrf.fr',
            /** HTTP port to use on the host side where graylog server is listening */
            port: 12205,
            /** Facility */
            facility: 'ESRF'
        },
        /** Logging to the terminal */
        console: {
            /** Level of verbosity. Use '0' for no logs ; Use 'silly' to see all logs */
            level: 'silly'
        },
        /** Logging to file */
        file: {
            /** If TRUE, logs are written to a file */
            enabled: true,
            /** Absolute filepath where the logs will be stored */
            filename: '/tmp/server.log',
            /** Verbosity level. See logging/console/level for details*/
            level: 'silly'
        }
    },

    /** Datacite configuration section. Datacite is an organization which creates DOIs (Digital Object Identifiers).
     *  We use this third-party services to create DOI for our data. More info [here](https://www.datacite.org/)*/
    datacite: {
        /** DOI prefix for your institution */
        prefix: '*****',
        /** Fixed part in the DOI suffix. */
        suffix: '*****',
        /** Username used for the authentication on datacite services */
        username: '*****',
        /** Password used for authentication on datacite services */
        password: '*****',
        /** Datacite metadata store (MDS) URL */
        mds: 'https://mds.datacite.org',
        /** Common part of the URL shared by all DOI landing pages */
        landingPage: 'https://doi.esrf.fr/',
        /** Proxi settings */
        proxy: {
            host: 'proxy.esrf.fr',
            port: '3128'
        }
    },
    /** ICAT data service configuration section */
    ids: {
        /** IDS server URL */
        server: 'https://ids.esrf.fr',
    },
    /** ICAT metadata catalogue configuration section */
    icat: {
        /** ICAT server URL */
        server: 'https://icat.esrf.fr',   //'https://ovm-icat-test:8181',
        /** Maximum number of events that can be returned from the elogbook by a single HTTP request */
        MAX_QUERY_LIMIT: 10000,
        /** Logbook configuration */
        events: {
            /** Permission management section*/
            permission: {
                /** All the groups which have permissions to see all logbook events. Usually concerns the administrator group. 
                 * The group is a concept defined in ICAT and must be defined on ICAT. */
                allowedGroups: ['admin'],
                /** ICAT connection settings of userGroupReader. This ICAT user has permission to consult the userGroup/user tables 
                 * on ICAT database. This way, UserGroupReader is used to find out who the administrators are. */
                userGroupReader: {
                    /** Authentication mechanism used by ICAT server */
                    plugin: "db",
                    /** Credentials of the userGroupReader */
                    credentials: [
                        { username: "**********" },
                        { password: "**********" }
                    ]
                }
            }
        },
        /** ICAT connection settings of minter. This ICAT user has the permission to create and read datacollections in the ICAT database. */
        minter: {
            /** Authentication mechanism used by ICAT server */
            plugin: "db",
            /** Credentials of the minter */
            credentials: [
                { username: "**********" },
                { password: "**********" }
            ]
        },
        /**  ICAT connection settings of notifier. This ICAT user needs to get access to all investigations in the ICAT database and it will 
         * convert investigationName and beamline into investigationId. */
        notifier: {
            /** Authentication mechanism used by ICAT server */
            plugin: "db",
            /** Credentials of the notifier */
            credentials: [
                { username: "**********" },
                { password: "**********" }
            ]
        },
    }
}