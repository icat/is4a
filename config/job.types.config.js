module.exports = { 
 types : {
    mx : {
        autoprocintegration : {
            dimple : {
                schema : {
                    "title" : "Dimple",
                    "description": "DIMPLE is a pipeline created primarily to process crystals that contain a known protein and possibly a ligand bound to this protein. But in principle can be used with any model.                    In the ligand screening scenario, the goal is to present a user with a quick answer to the question of whether or not they have a bound ligand or drug candidate in their crystal.",
                    "type": "object",
                    "properties": {                      
                        "pdbFile" : {
                            "type": "file",                            
                            "name": "PDB file",
                            "description": "A PDB file that represents the structure of the molecule"
                        }
                    }
                }
            }
        },
        datacollectiongroup : {
            add : {
                schema : {
                    "title" : "Test",
                    "description": "Adds two integers",
                    "type": "object",
                    "properties": {
                        "a": {
                            "type": "number"
                        },
                        "b": {
                            "type": "number"
                        },
                        "Example File" : {
                            "type": "file",
                            "name": "PDB file",
                            "description": "A PDB file that represents the structure of the molecule"
                        }
                    }
                }
            },
            subtract : {
                schema : {
                    "title" : "Subtract",
                    "description": "Adds two integers",
                    "type": "object",
                    "properties": {
                        "a": {
                            "type": "number"
                        },
                        "b": {
                            "type": "number"
                        }
                    }
                }
            }  
        },
        datacollection: {
            reprocess: {
                schema : {
                    "title": "Reprocessing Dataset",
                    "description": "Fill this form and click on submit to launch the job",
                    type : "object",            
                    "required": [
                        "type"
                    ],
                    "properties": {
                        "type": {
                            "title": "Select Pipeline",
                            "type": "string",
                            "enum": [
                                "EDNA_proc",
                                "autoPROC",
                                "XIA2_dials",
                                "grenades_fastproc",
                                "grenades_parallelproc"
                            ],
                            "enumNames": [
                                "EDNA_proc",
                                "autoPROC",
                                "XIA2_dials",
                                "grenades_fastproc",
                                "grenades_parallelproc"
                            ]
                        },

                        "Start": {
                            "type": "number"
                        },
                        "End": {
                            "type": "number"
                        },
                        "Space Group": {
                            "title": "Force Space Group",
                            "type": "string"
                        },
                        "Resolution": {
                            "type": "string",
                            "title": "Resolution cut-off"
                        },
                        "Anomalous": {
                            "type": "boolean",
                            "title": "Anomalous"
                        }
                    }
                }
            }
        },
        datacollections : {
            Merge : {
                schema : {
                    title: "Merge",
                    description : "Merge of two or more datasets by using XSCALE",
                    type : "object",                    
                    properties: {                                       
                    }
                },
                uischema : {                    
                },
                formdata : {                    
                }
            }               
        }
    }
 }
}