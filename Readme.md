# IS4A

IS4A is a NodeJS package that exposes a RestFul API and uses a MongoDB database. It allows:
- Store information of jobs 
- Store results


IS4A+ is a collaborative project and licensed under the MIT license.


# Menu
1. [Installation](#installation)
2. [Configuration](#configuration)
3. [API](#api)
4. [Database](#Database)

## Installation

1. Download the source code from github repository
2. Install dependencies

```bash
npm install
```

3. A MongoDB instance is required. The following command will download and install a docker image containing mongoDB database. Then it will start a mongoDB instance listening at port 27017.

```
docker run  -p 27017:27017 mongo
```

4. Install the PDF generator (**optional and only needed for elogbook reporting**)

For Debian it is necessary to install wkhtmltopdf as described [here](https://wkhtmltopdf.org/downloads.html). It seems that the version they provided has more features than the one provided by apt-get in the central Debian repositories.

```
wget https://downloads.wkhtmltopdf.org/0.12/0.12.5/wkhtmltox_0.12.5-1.stretch_amd64.deb
dpkg -i wkhtmltox_0.12.5-1.stretch_amd64.deb
```

5. Run

```bash
npm start
```

Or Run with nodemon. This will update the server automatically


```bash
 nodemon npm start
```

# API
<!--START api-docs -->
<table><tr><td>Path</td><td>Method</td><td>Summary</td></tr><tr><td>/jobs</td><td>GET</td><td>Returns all jobs</td></tr><tr><td>/jobs</td><td>POST</td><td>Creates a new job</td></tr><tr><td>/jobs/type/datacollection</td><td>GET</td><td>Returns json schema of jobs that can be run for a data collection</td></tr><tr><td>/jobs/type/datacollectiongroup</td><td>GET</td><td>Returns json schema of jobs that can be run for a data collection</td></tr><tr><td>/jobs/{id}/output</td><td>POST</td><td>Add output to the job</td></tr><tr><td>/jobs/{id}/status/{status}</td><td>GET</td><td>Returns allowed types of job</td></tr><tr><td>/jobs/{username}</td><td>GET</td><td>Gets all jobs from user</td></tr></table>

<!--END api-docs -->

# Database
<!--START mongoDoc -->
```js
/**
 * MongoDB schema
 */
/** @module mongoDBSchema */

const mongoose = require('mongoose');

/** Event schema */
const JobSchema = mongoose.Schema({       
    /** Type of the job */
    type: String,
    /** Aunthentication token */
    token: String,
     /** Aunthentication token */
    username: String,
    /** status of the job */
    status: String,       
    /** Job's input */
    input : mongoose.Schema.Types.Mixed,
    /** Job's output */
    output : mongoose.Schema.Types.Mixed    
}, {
    timestamps: true
});

module.exports = mongoose.model('Job', JobSchema);


```
<!--END mongoDoc -->
